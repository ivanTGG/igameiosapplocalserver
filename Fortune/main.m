//
//  main.m
//  Fortune
//
//  Created by Ivan Wan on 6/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
