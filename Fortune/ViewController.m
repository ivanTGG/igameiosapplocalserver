//
//  ViewController.m
//  Fortune
//
//  Created by Ivan Wan on 6/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

// Note: Steps to build app to load game from local server:
//
// - Run local game server on another Macbook, sharing a local network
// - Have an iPad (or iPhone) connected to this local network
//
// - Build app to this iPad:
//    - choose the "target" to build on top bar (e.g. 777)
//    - uncomment the correct game path below (e.g. first link below is for 777)
// - Now it's ready to build the game you choose

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* url;
    
//    url= @"http://192.168.2.1/777/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
    url= @"http://192.168.2.1/blackjack_impossible/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/extra_cash/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/flamenco_dance/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/fortune_guardians/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/fruity_bites/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/galaxy/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/goa2/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/happy_farm/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/king_of_pirate/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/legand_of_cairo/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/mahjong_impossible/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/monster_battleground/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/opal/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/robyn/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/rolling_gold/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/three_kingdoms/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/todays_weather/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
//    url= @"http://192.168.2.1/wild_rose/?serverMetadataURL=http://192.168.2.1:8088/config&debugger=1";
    
//    Testing use: calling real server
    url = @"http://igaming-prototype.s3-website-ap-southeast-1.amazonaws.com/games/fortune_guardians_igaming/?debugger=1&serverMetadataURL=http://igaming-rgs-stg.tgggames.com:8088/config";

    
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request=[NSURLRequest requestWithURL:nsUrl];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
